#Ansible | Create Role and deploy it on Private Ansible Galaxy

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Notre plan d'action vise à améliorer la collaboration avec une autre équipe en transformant notre playbook webapp en un rôle modulaire pour une adaptation facile, en intégrant un playbook de tests pour une évaluation et une intégration rapides dans les processus de déploiement, et en créant une galaxy privée pour une gestion centralisée des rôles développés. Ces étapes renforceront notre workflow et notre capacité à répondre aux besoins spécifiques de l'équipe.

## Objectifs

Dans ce lab, nous allons :

- Transformer notre playbook webapp en un rôle. Cette étape permettra une modularité accrue, offrant ainsi la flexibilité nécessaire pour une adaptation sur mesure.

- Intégrer un playbook de tests (wordpress) au rôle pour faciliter son évaluation rapide et son intégration dans le processus de déploiement existant.

- Provisionner une instance cible ec2 à l'aide de terraform et configurer cette instance à l'aide d'ansible et y deployer le nouveau playbook en guise de test

- Établir une galaxy privée destinée à la conservation et à la gestion centralisée des rôles développés, renforçant l'efficacité et la réutilisation des ressources au sein de notre entreprise.

Ces actions permettront d'optimiser notre workflow et de répondre aux exigences spécifiques de l'équipe.


## Prérequis
Disposer d'un machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu grace au provider AWS, à partir duquel nous effectuerons toutes nos configurations Ansible.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)


## Arboréscence du projet


*Arborescence du projet*

## 1. Refactorisation de notre playbook initial en rôle

Rappellons que nous avons mis en place un playbook permettant de lancer un conteneur apache 
